package com.company;

import java.util.concurrent.RecursiveAction;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

/**
 * Created by heinr on 13.11.2016.
 */
public class ForkAction extends RecursiveAction {

    final private int startPosition;
    final private int partLenght;

    static private int partLenghtMax;
    static private int taskCount;
    static private int drob;

    static private int half = ImageBlurWithExecutorService.getmBlurWidth() / 2;
    static private int[][] square;
    static private int[][] result;

    public ForkAction(int startPosition, int partLenght) {
        this.startPosition = startPosition;
        this.partLenght = partLenght;
    }

    public static void setSquare(int[][] square, int taskCount) {

        ForkAction.partLenghtMax = square.length;
        ForkAction.taskCount = taskCount;

        ForkAction.square = new int[ForkAction.partLenghtMax][ForkAction.partLenghtMax];
        ForkAction.result = new int[ForkAction.partLenghtMax][ForkAction.partLenghtMax];

        drob = ForkAction.partLenghtMax / taskCount;

        for (int i = 0; i < ForkAction.partLenghtMax; i++) {
            for (int j = 0; j < ForkAction.partLenghtMax; j++) {
                ForkAction.square[i][j] = square[i][j];
                ForkAction.result[i][j] = 0;
            }
        }
    }

    public int[][] getResult() {
        return result;
    }

    public static void setTaskCount(int taskCount) {
        ForkAction.taskCount = taskCount;
    }

    @Override
    protected void compute() {
        if (this.partLenght > drob) {

            int partLenght_1 = partLenght / 2;

            int startPosition_2 = startPosition + partLenght_1;
            int partLenght_2 = partLenght - partLenght_1;

            ForkAction forkAction1 = new ForkAction(startPosition, partLenght_1);
            forkAction1.fork();

            ForkAction forkAction2 = new ForkAction(startPosition_2, partLenght_2);
            forkAction2.fork();

            forkAction1.join();
            forkAction2.join();

        } else {
            for (int i = 0; i < square.length; i++) {
                for (int j = startPosition; j < startPosition + partLenght; j++) {

                    int minHorizont = max(0, i - half);
                    int maxHorizont = min(square.length, i + half);
                    int lenghtHorizont = maxHorizont - minHorizont;
                    int minVert = max(0, j - half);
                    int maxVert = min(square.length, j + half);
                    int lenghtVert = maxVert - minVert;

                    int[][] blur = new int[lenghtHorizont][lenghtVert];

                    for (int k = 0; k < lenghtHorizont; k++) {
                        for (int l = 0; l < lenghtVert; l++) {
                            blur[k][l] = square[minHorizont + k][minVert + l];
                        }
                    }
                    //размытие пикселя
                    int resultRed = 0;
                    int resultBlue = 0;
                    int resultGreen = 0;
                    int arraySize = lenghtHorizont * lenghtVert;

                    for (int bl_H = 0; bl_H < lenghtHorizont; bl_H++) {
                        for (int bl_V = 0; bl_V < lenghtVert; bl_V++) {

                            resultGreen = resultGreen + ((blur[bl_H][bl_V] & 0x00_00_FF_00) >> 8);
                            resultRed = resultRed + ((blur[bl_H][bl_V] & 0x00_FF_00_00) >> 16);
                            resultBlue = resultBlue + (blur[bl_H][bl_V] & 0x00_00_00_FF);
                        }
                    }
                    result[i][j] = 0xFF_00_00_00 | (resultBlue / arraySize) | ((resultGreen / arraySize) << 8) | ((resultRed / arraySize) << 16);
 //                   result[i][j] = square[i][j];
                }
            }
        }
    }

}
